package com.example.demo.exception;

//@ResponseStatus(HttpStatus.NOT_FOUND)
public class EstudianteNotFoundException extends RuntimeException{
    public EstudianteNotFoundException(String message) {
        super(message);
    }
}
