package com.example.demo.exception;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExceptionResponse {
    private Date timestamp;
    private String mensaje;
    private String detalles;
    private String httpCodeMessage;

}
