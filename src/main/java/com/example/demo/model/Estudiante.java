package com.example.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
// import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "estudiante")
public class Estudiante {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "identificacion")
	private long identificacion;

	@Column(name = "correo")
	private String correo;

	@Column(name = "edad")
	private int edad;

	// @Column(name = "estado")
	// @Enumerated(value = EnumType.STRING)
	// private Estado estado;

	@Column(name = "estado")
	private String estado;

	@Column(name = "sexo")
	private char sexo;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "acudienteId")
	private Acudiente acudiente;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "matriculaId")
	private Matricula matricula;

	public void addMatricula(Matricula matricula) {
		matricula.setEstudiante(this);
		this.matricula = matricula;
	}

	public void addAcudiente(Acudiente acudiente) {
		acudiente.setEstudiante(this);
		this.acudiente = acudiente;
	}

	public void removeMatricula() {
		if (matricula != null) {
			matricula.setEstudiante(null);
			this.matricula = null;
		}
	}

	public void removeAcudiente() {
		if (acudiente != null) {
			acudiente.setEstudiante(null);
			this.acudiente = null;
		}
	}
}
