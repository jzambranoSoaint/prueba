package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "matricula")
public class Matricula {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "fechaMatricula")
	private String fechaMatricula;

	@Column(name = "radicado")
	private String radicado;

	@JsonIgnore
	@OneToOne(mappedBy = "matricula", fetch = FetchType.LAZY)
	private Estudiante estudiante;
}
