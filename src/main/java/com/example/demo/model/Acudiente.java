package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "acudiente")
public class Acudiente {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "cedula")
	private long cedula;

	@Column(name = "celular")
	private long celular;

	@Column(name = "direccion")
	private String direccion;

	@JsonIgnore
	@OneToOne(mappedBy = "acudiente", fetch = FetchType.LAZY, orphanRemoval = true)
	private Estudiante estudiante;
}
