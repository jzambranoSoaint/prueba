package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import com.example.demo.exception.EstudianteNotFoundException;
import com.example.demo.model.Estudiante;
import com.example.demo.service.EstudianteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "estudianteapi", description = "Métodos para hacer uso de la entidad estudiante")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EstudianteController {
	@Autowired
	private EstudianteService estudianteService;

	@ApiOperation(value = "Agrega un estudiante a la BBDD si es menor de 18 años", response = Estudiante.class)
	@PostMapping(value = "/estudiante")
	private ResponseEntity<Estudiante> guardar(@RequestBody Estudiante estudiante) throws EstudianteNotFoundException {
		Estudiante aux = estudianteService.create(estudiante);
		return new ResponseEntity<>(aux, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Obtiene un estudiante por ID", response = Estudiante[].class)
	@ApiResponses(value = { @ApiResponse(code = 302, message = "Estudiante obtenido correctamente"),
			@ApiResponse(code = 406, message = "El estudiante no fue encontrado") })
	@GetMapping(value = "/estudiante/{id}")
	private ResponseEntity<Optional<Estudiante>> obtener(@PathVariable Long id) throws EstudianteNotFoundException {
		Optional<Estudiante> aux = estudianteService.findById(id);
		return new ResponseEntity<>(aux, HttpStatus.FOUND);
	}

	@ApiOperation(value = "Obtiene todos los estudiantes")
	@GetMapping(value = "/estudiante")
	private ResponseEntity<List<Estudiante>> obtenerTodos() {
		List<Estudiante> aux = estudianteService.findAll();
		return new ResponseEntity<>(aux, HttpStatus.OK);
	}

	@ApiOperation(value = "Obtiene los estudiante filtrados por estado 'activo' y por sexo")
	@GetMapping(value = "/estudiante/{estado}/{sexo}")
	private ResponseEntity<List<Estudiante>> porEstadoSexo(@PathVariable String estado, @PathVariable char sexo) {
		List<Estudiante> aux = estudianteService.findByEstadoAndSexo(estado, sexo);
		return new ResponseEntity<>(aux, HttpStatus.OK);
	}

	@ApiOperation(value = "Actualiza un estudiante por el ID")
	@PutMapping(value = "/estudiante/{id}")
	private ResponseEntity<Estudiante> actualizar(@PathVariable Long id, @RequestBody Estudiante estudiante) {
		Estudiante aux = estudianteService.update(id, estudiante);
		return new ResponseEntity<>(aux, HttpStatus.OK);
	}

	@ApiOperation(value = "Cambia el estado de un estudiante a 'inactivo'")
	@DeleteMapping(value = "/estudiante/{id}")
	private ResponseEntity<String> eliminar(@PathVariable Long id) {
		estudianteService.delete(id);
		return new ResponseEntity<>("Estado del estudiante inactivo", HttpStatus.OK);
	}
}
