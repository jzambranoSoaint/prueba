package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.exception.EstudianteNotFoundException;
// import com.example.demo.model.Estado;
import com.example.demo.model.Estudiante;
import com.example.demo.repository.EstudianteRepository;

@Service
public class EstudianteService {
	@Autowired
	private EstudianteRepository estudianteRepository;

	public Estudiante create(Estudiante estudiante) throws EstudianteNotFoundException {
		if (!(estudiante.getEdad() < 18)) {
			throw new EstudianteNotFoundException("El estudiante debe ser menor de 18 años");
		}
		estudiante.addMatricula(estudiante.getMatricula());
		estudiante.addAcudiente(estudiante.getAcudiente());
		return estudianteRepository.save(estudiante);
	}

	public Optional<Estudiante> findById(Long id) throws EstudianteNotFoundException {
		Optional<Estudiante> estudiante = estudianteRepository.findById(id);
		if (!estudiante.isPresent()) {
			throw new EstudianteNotFoundException("Estudiante " + id + " no encontrado");
		}
		return estudiante;
	}

	public List<Estudiante> findAll() {
		return estudianteRepository.findAll();
	}

	public Estudiante update(Long id, Estudiante nuevoEstudiante) throws EstudianteNotFoundException {
		Optional<Estudiante> aux = estudianteRepository.findById(id);

		if (!aux.isPresent()) {
			throw new EstudianteNotFoundException("El estudiante " + id + " no existe para actualización");
		}

		Estudiante estudiante = aux.get();
		estudiante.setNombre(nuevoEstudiante.getNombre());
		estudiante.setIdentificacion(nuevoEstudiante.getIdentificacion());
		estudiante.setCorreo(nuevoEstudiante.getCorreo());
		estudiante.setEdad(nuevoEstudiante.getEdad());
		estudiante.setEstado(nuevoEstudiante.getEstado());
		estudiante.setSexo(nuevoEstudiante.getSexo());
		estudianteRepository.save(estudiante);
		return estudiante;

	}

	public Estudiante delete(Long id) {
		Optional<Estudiante> aux = estudianteRepository.findById(id);

		if (!aux.isPresent()) {
			throw new EstudianteNotFoundException("El estudiante " + id + " no encontrado para inactivación");
		}

		Estudiante estudiante = aux.get();
		estudiante.setEstado("inactivo");
		estudiante.removeMatricula();
		estudianteRepository.save(estudiante);
		return estudiante;
	}

	public List<Estudiante> findByEstadoAndSexo(String estado, char sexo) {
		if (estado.equals("activo")) {
			return estudianteRepository.findByEstadoAndSexo(estado, sexo);
		}
		return estudianteRepository.findByEstadoAndSexo(null, sexo);
	}
}
