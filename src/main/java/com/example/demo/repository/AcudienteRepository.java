package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Acudiente;

public interface AcudienteRepository extends JpaRepository<Acudiente, Long>{

}
