package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Estudiante;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long>{
	public List<Estudiante> findByEstadoAndSexo(String estado, char sexo);
}
